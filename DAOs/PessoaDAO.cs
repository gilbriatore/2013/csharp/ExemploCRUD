﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using Dominio;

namespace DAOs
{
    public class PessoaDAO
    {
        //String que guarda as informações de conexão com o banco,
        //seus dados estão no arquivo app.config.            
        string strConexao = ConfigurationManager.ConnectionStrings["ConexaoExemploCRUD"].ConnectionString;

        public void Incluir(Pessoa p)
        {
            //Query que será executada no banco de dados.
            string query = @"INSERT INTO [ExemploCRUD].[dbo].[pessoa]
                                   ([nome]
                                   ,[sobrenome])
                             VALUES
                                   (@nome
                                   ,@sobrenome)";

            
            //Objeto responsável por abrir a conexão com o banco.
            SqlConnection conexao = new SqlConnection(strConexao);

            //Parâmetros que serão usados na execução da query.
            SqlParameter p1 = new SqlParameter("nome", p.Nome);
            SqlParameter p2 = new SqlParameter("sobrenome", p.Sobrenome);

            //Objeto que executa o comando no banco de dados.
            SqlCommand comando = conexao.CreateCommand();
            
            //Atribuição da query no comando que será executado.
            comando.CommandText = query;

            //Adição dos parâmetros no comando.
            comando.Parameters.Add(p1);
            comando.Parameters.Add(p2);
            
            //Abre a conexão.
            conexao.Open();
            //Executa a query.
            comando.ExecuteNonQuery();
            //Fecha a conexão.
            conexao.Close();
        }

        public Pessoa Pesquisar(int id)
        {
            Pessoa pessoa = null;
            try
            {
               
                SqlConnection conexao = new SqlConnection(strConexao);
                SqlCommand comando = conexao.CreateCommand();
                comando.CommandText = @"SELECT * FROM pessoa p WHERE p.pessoa_id= @id";
                SqlParameter p = new SqlParameter("id", id);
                comando.Parameters.Add(p);
                conexao.Open();
                SqlDataReader leitor = comando.ExecuteReader();
                while (leitor.Read())
                {
                    pessoa = new Pessoa();
                    pessoa.Nome = (String)leitor["nome"];
                    pessoa.Sobrenome = (String)leitor["sobrenome"];
                }
                conexao.Close();
            }
            catch (Exception)
            {
               //tratamento do erro                
            }
            return pessoa;
        }

        public List<Pessoa> Listar()
        {            
            List<Pessoa> lista = new List<Pessoa>();
            SqlConnection conexao = new SqlConnection(strConexao);
            SqlCommand comando = conexao.CreateCommand();
            comando.CommandText = @"SELECT * FROM pessoa p";
            conexao.Open();
            SqlDataReader leitor = comando.ExecuteReader();
            while(leitor.Read()) {
                Pessoa pessoa= new Pessoa();
                pessoa.Nome= (String) leitor["nome"];
                pessoa.Sobrenome= (String) leitor["sobrenome"];
                lista.Add(pessoa);
            }
            conexao.Close();
            return lista;
        }

        public void Atualizar(Pessoa pessoa)
        {
            
            SqlConnection conexao = new SqlConnection(strConexao);
            SqlCommand comando = conexao.CreateCommand();
            comando.CommandText = @"UPDATE pessoa SET sobrenome = @sobrenome WHERE nome = @nome";
            SqlParameter p1 = new SqlParameter("sobrenome", pessoa.Sobrenome);
            SqlParameter p2 = new SqlParameter("nome", pessoa.Nome);
            comando.Parameters.Add(p1);
            comando.Parameters.Add(p2);
            conexao.Open();
            comando.ExecuteNonQuery();
            conexao.Close();
        }

        public void Excluir(Pessoa pessoa)
        {
            
            SqlConnection conexao = new SqlConnection(strConexao);
            SqlCommand comando = conexao.CreateCommand();
            comando.CommandText = @"DELETE FROM pessoa WHERE nome = @nome";
            SqlParameter p1 = new SqlParameter("nome", pessoa.Nome);
            comando.Parameters.Add(p1);
            conexao.Open();
            comando.ExecuteNonQuery();
            conexao.Close();
        }
    }
}
