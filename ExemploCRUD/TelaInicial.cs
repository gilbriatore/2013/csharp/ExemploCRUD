﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Dominio;
using Servicos;

namespace ExemploCRUD
{
    public partial class TelaInicial : Form
    {
        CadastroDePessoa cadastro = new CadastroDePessoa();

        public TelaInicial()
        {
            InitializeComponent();
        }

        private void btnIncluir_Click(object sender, EventArgs e)
        {
            Pessoa p = new Pessoa();
            p.Nome = txtNome.Text;
            p.Sobrenome = txtSobrenome.Text;
            cadastro.Incluir(p);
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            Pessoa pessoa = cadastro.Pesquisar(1);
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            List<Pessoa> lista = cadastro.Listar();
        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            Pessoa pessoa = new Pessoa();
            cadastro.Atualizar(pessoa);
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            Pessoa pessoa = new Pessoa();
            cadastro.Excluir(pessoa);
        }
    }
}
