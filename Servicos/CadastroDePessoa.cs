﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAOs;
using Dominio;

namespace Servicos
{
    public class CadastroDePessoa
    {
        PessoaDAO dao = new PessoaDAO();

        public void Incluir(Dominio.Pessoa p)
        {            
            dao.Incluir(p);
            
        }

        public Pessoa Pesquisar(int id)
        {
            return dao.Pesquisar(id);
        }

        public List<Pessoa> Listar()
        {
            return dao.Listar();
        }

        public void Atualizar(Pessoa pessoa)
        {
            dao.Atualizar(pessoa);
        }

        public void Excluir(Pessoa pessoa)
        {
            dao.Excluir(pessoa);
        }
    }
}
